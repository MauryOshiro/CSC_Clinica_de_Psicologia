package br.com.maury.clinicaCristiano.atendimento.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.maury.clinicaCristiano.atendimento.modelo.Profissional;

public class ProfissionalDAO {
	private Connection con;

	public Connection getConnection() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}
	
	public ProfissionalDAO(Connection con) {
		this.con = con;
	}

	public Boolean gravar(Profissional p) {
		String sql = "INSERT INTO tb_Profissional (crp,tb_Funcionario_id) "
				+ "VALUES(?,?)";
		
		try (PreparedStatement pstm = getConnection().prepareStatement(sql,Statement.RETURN_GENERATED_KEYS)) {
			pstm.setString(1, p.getCrp());
			pstm.setInt(2, p.getIdFuncionario());
			pstm.execute();
			
			try (ResultSet rsgk = pstm.getGeneratedKeys()) {
				if(rsgk.next()) {
					p.setIdProfissional(rsgk.getInt(1));
					return true;	
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
}
