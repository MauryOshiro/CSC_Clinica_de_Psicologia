package br.com.maury.clinicaCristiano.atendimento.modelo;

public class AgendaFuncionarioEmpresa extends AgendaCompromisso{
	FuncionarioEmpresa funcionario;
	Boolean faturado;

	public Boolean getFaturado() {
		return faturado;
	}

	public void setFaturado(Boolean faturado) {
		this.faturado = faturado;
	}

	public FuncionarioEmpresa getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(FuncionarioEmpresa funcionario) {
		this.funcionario = funcionario;
	}
}
