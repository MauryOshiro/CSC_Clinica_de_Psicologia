package br.com.maury.clinicaCristiano.atendimento.modelo;

public class AgendaPaciente extends AgendaCompromisso {
	Paciente paciente;
	Boolean geradoGuia;

	public Boolean getGeradoGuia() {
		return geradoGuia;
	}

	public void setGeradoGuia(Boolean geradoGuia) {
		this.geradoGuia = geradoGuia;
	}

	public Boolean getFaturado() {
		return faturado;
	}

	public void setFaturado(Boolean faturado) {
		this.faturado = faturado;
	}

	Boolean faturado;

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
}