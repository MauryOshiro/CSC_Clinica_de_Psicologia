package br.com.maury.clinicaCristiano.atendimento.modelo;

public class Prontuario {
	Integer id;
	String prontuario;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProntuario() {
		return prontuario;
	}

	public void setProntuario(String prontuario) {
		this.prontuario = prontuario;
	}
}
