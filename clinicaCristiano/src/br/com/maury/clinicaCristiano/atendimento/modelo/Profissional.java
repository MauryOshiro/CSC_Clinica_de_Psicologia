package br.com.maury.clinicaCristiano.atendimento.modelo;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Collection;

import br.com.maury.clinicaCristiano.sistema.modelo.Usuario;

public class Profissional extends Funcionario {
	Integer id;
	String crp;

	public Profissional(String nome, String sobrenome, String rg, BigInteger cpf, LocalDate dataNascimento,
			String observacao, Endereco endereco, ContatoPessoaFisica contato, String sexo, String nivelEscolaridade,
			Integer idade, String departamento, Cargo cargo, LocalDate dataAdmissao, Boolean ativo, Usuario usuario, 
			Collection<UnidadeDaEmpresa> unidades,String crp) {
		super(nome,sobrenome,rg,cpf,dataNascimento,observacao,endereco,contato,sexo,nivelEscolaridade,idade,
				departamento,cargo,dataAdmissao,ativo,usuario,unidades);
		this.crp = crp;
		
	}

	public Integer getIdProfissional() {
		return id;
	}

	public void setIdProfissional(Integer id) {
		this.id = id;
	}

	public String getCrp() {
		return crp;
	}

	public void setCrp(String crp) {
		this.crp = crp;
	}
}
