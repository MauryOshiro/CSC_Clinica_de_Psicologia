package br.com.maury.clinicaCristiano.atendimento.modelo;

public class ExameConclusao {
	Integer id;
	String conclusao;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getConclusao() {
		return conclusao;
	}

	public void setConclusao(String conclusao) {
		this.conclusao = conclusao;
	}
}
