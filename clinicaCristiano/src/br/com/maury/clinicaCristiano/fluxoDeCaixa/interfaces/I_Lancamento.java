package br.com.maury.clinicaCristiano.fluxoDeCaixa.interfaces;

public interface I_Lancamento {
	public void cancelar();
	
	public void ativar();
	
	public void editar();
}
