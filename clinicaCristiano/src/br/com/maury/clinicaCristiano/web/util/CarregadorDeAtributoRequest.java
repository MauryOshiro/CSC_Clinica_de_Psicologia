package br.com.maury.clinicaCristiano.web.util;

import java.sql.Connection;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import br.com.maury.clinicaCristiano.atendimento.dao.CargoDAO;
import br.com.maury.clinicaCristiano.atendimento.dao.UnidadeDaEmpresaDAO;
import br.com.maury.clinicaCristiano.atendimento.modelo.Cargo;
import br.com.maury.clinicaCristiano.atendimento.modelo.UnidadeDaEmpresa;
import br.com.maury.clinicaCristiano.jdbc.ConnectionPool;
import br.com.maury.clinicaCristiano.sistema.dao.ModuloDAO;
import br.com.maury.clinicaCristiano.sistema.modelo.Modulo;

public class CarregadorDeAtributoRequest {
	public static void buscaModulos(HttpServletRequest req) {
		try (Connection con = ConnectionPool.getConnection()) {
			ModuloDAO moduloDAO = new ModuloDAO(con);
			Collection<Modulo> modulos = moduloDAO.buscaModulos();
			req.setAttribute("modulos", modulos);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void buscaUnidadesDaEmpresa(HttpServletRequest req) {
		try (Connection con = ConnectionPool.getConnection()) {
			UnidadeDaEmpresaDAO unidadeDAO = new UnidadeDaEmpresaDAO(con);
			Collection<UnidadeDaEmpresa> unidades = unidadeDAO.buscaUnidadeDaEmpresa();
			req.setAttribute("unidadesDaEmpresa", unidades);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void buscaCargo(HttpServletRequest req) {
		try (Connection con = ConnectionPool.getConnection()) {
			CargoDAO cargoDAO = new CargoDAO(con);
			Collection<Cargo> cargos = cargoDAO.buscaCargos();
			req.setAttribute("cargos", cargos);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
